<?php
  require_once('Animal.php');
  require_once('Frog.php');
  require_once('Ape.php');

  $sheep = new Animal('shaun');

  echo $sheep->name; // "shaun"
  echo $sheep->legs; // 2
  echo $sheep->cold_blooded; // false

  $sungokong = new Ape("kera sakti");
  echo $sungokong->name; // "shaun"
  echo $sungokong->legs; // 2
  echo $sungokong->cold_blooded; // false
  $sungokong->yell(); // "Auooo"

  $kodok = new Frog("buduk");
  echo $kodok->name; // "shaun"
  echo $kodok->legs; // 2
  echo $kodok->cold_blooded; // false
  $kodok->jump() ; // "hop hop"
 ?>
